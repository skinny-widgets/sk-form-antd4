
import { AntdSkForm }  from '../../sk-form-antd/src/antd-sk-form.js';

export class Antd4SkForm extends AntdSkForm {

    get prefix() {
        return 'antd4';
    }

}
